To get Nginx going you you'll need a cert and key (in the ssl subdirectory), and to be extra secure you should also generate a dhparam something like this:

	 openssl dhparam -out /srv/persistent-data/docker-scripts/nginx/ssl/dhparam.pem 4096

In addition, you need to edit one line in the file 

	default.conf

Update this line to have your server name:

    server_name YOUR_HOSTNAME_GOES_HERE;


